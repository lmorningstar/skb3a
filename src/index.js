import express from 'express';
import fetch from 'isomorphic-fetch';
import Promise from 'bluebird';
import _ from 'lodash';
import cors from 'cors';

const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

let pc = {};

const DEV = true;

const app = express();

app.use(cors());

function getAll() {
  return pc;
}

function getMainParam(field0, field1, field2) {
  console.log(field0, field1, field2);
  let answer = {}
  if (field0 in pc) {
    if (!field1) {
      answer = { status: 200, body: pc[field0] };
    } else {
      if(!field2) {
        if (field1 in pc[field0]) {
          const mainField = pc[field0];
          answer = { status: 200, body: mainField[field1] }
        } else {
          answer = { status: 404, body: 'Not found' }
        }
      } else {
        const mainField = pc[field0];
        const subField = mainField[field1];
        if (field2 in subField) {
          answer = { status: 200, body: subField[field2] }
        } else {
          answer = { status: 404, body: 'Not found' }
        }
      }
    }
  } else {
    answer = { status: 404, body: 'Not found' }
  }
  return answer;
}

app.get('/', (req, res) => {
  res.json(getAll());
});


app.get('/volumes', (req, res) => {
  let result = {
    'C:': 0,
    'D:': 0,
  };
  let finResult = {
    'C:': '',
    'D:': '',
  }
  const hdd = pc.hdd;
  hdd.forEach((volume) => {
    if (volume.volume === 'C:') {
      result['C:'] += volume.size;
    } else {
      result['D:'] += volume.size;
    }
  });
  finResult['C:'] = result['C:'] + 'B';
  finResult['D:'] = result['D:'] + 'B';

  res.status(200);
  res.json(finResult);
});


app.get('/:field0?/:field1?/:field2?', (req, res) => {
  console.log(req.params);
  const field0 = req.params.field0;
  const field1 = req.params.field1;
  const field2 = req.params.field2;
  const result = getMainParam(field0, field1, field2);
  if (result.status === 200) {
    res.json(result.body);
  } else {
    res.status(result.status);
    res.send(result.body);
  }
});

app.listen(3000, () => {
  fetch(pcUrl)
    .then(async (res) => {
      pc = await res.json();
      //console.log(pc);
    })
    .catch((err) => {
      console.log('Чтото пошло не так:', err);
    });
  console.log('App listening on port 3000!');
});

/*
{
  "board":{
    "vendor":"IBM",
    "model":"IBM-PC S-100",
    "cpu":{
      "model":"80286",
      "hz":12000
    },
    "image":"http://www.s100computers.com/My%20System%20Pages/80286%20Board/Picture%20of%2080286%20V2%20BoardJPG.jpg",
    "video":"http://www.s100computers.com/My%20System%20Pages/80286%20Board/80286-Demo3.mp4"},
    "ram":{
      "vendor":"CTS",
      "volume":1048576,
      "pins":30
    },
    "os":"MS-DOS 1.25",
    "floppy":0,
    "hdd":[
      {
        "vendor":"Samsung",
        "size":33554432,
        "volume":"C:"
      },
      {
        "vendor":"Maxtor",
        "size":16777216,
        "volume":"D:"
      },
      {
        "vendor":"Maxtor",
        "size":8388608,
        "volume":"C:"
      }
    ],
    "monitor":null
}
*/
